package com.example.mitstv;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

class DB extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "MITSTVDB";
    private static final int DATABASE_VERSION = 1;
    
    public DB(Context context) {
      super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
      Log.d("DB_HELPER", "--- onCreate database ---");
      db.execSQL("CREATE TABLE channels (cid TEXT PRIMARY KEY, name TEXT NOT NULL, prio INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
 
    }
  }