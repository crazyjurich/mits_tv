package com.example.mitstv;

import java.net.UnknownHostException;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.net.Uri;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class MainActivity extends Activity {
	protected static final String LOG_TAG = "MitsTV_main";
	protected static final String LAST_CHAN_UPDATE = "LAST_CHAN_UPDATE";
	protected static final long MIN_CHAN_UPDATE_INTERVAL = 60*60*1000; //milliseconds
	protected TextView errorText;
	protected ListView channelList;

	DB dbh;
	SharedPreferences pref;

	protected String doDownload(String url, String label) throws Exception {
		AsyncTaskResult<String> res = new downloadTask(this, label).execute(url).get();
		if (res.hasResult()){
			errorText.setText("");
			return res.getResult();
		}else{
			Exception e = res.getException();
			if (e instanceof UnknownHostException){
				channelList.setAdapter(null);
				errorText.setText("You are not on MiTs network");
			}
			throw e;
		}
	}

	protected String getVideoUrl(String chanId) throws Exception{
		String url = "http://tv.mits.lv/index.php?q=" + chanId;
		String pf = doDownload(url, "Loading channel data");
		//Log.d("MitsTV", "Playlist file:" + pf);
		if (!pf.startsWith("#EXTM3U")){
			throw new Exception("Wrong playlist file received: " + pf);
		}
		Pattern p = Pattern.compile("^http://.*$", Pattern.MULTILINE);
		Matcher m = p.matcher(pf);
		if (m.find()){
			return m.group(0);
		}else{
			throw new Exception("Wrong playlist file received: " + pf);
		}
	}

	protected void play(String url){
		//https://archive.org/download/ksnn_compilation_master_the_internet/ksnn_compilation_master_the_internet_512kb.mp4
		try {
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setPackage("org.videolan.vlc.betav7neon");
			i.setDataAndType(Uri.parse(url), "video/*");
			startActivity(i);
		} catch (ActivityNotFoundException e){
		    AlertDialog.Builder builder = new AlertDialog.Builder(this);
		    builder.setTitle("VLC player not found");
		    builder.setMessage("VLC player needed to play this content is not installed on this device.\nDo you want to install VLC player");
		    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int id) {
		            Intent viewIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=org.videolan.vlc.betav7neon"));
                    startActivity(viewIntent);
		       }
		    });
		    builder.setNegativeButton("Cancel", null);
		    builder.show();
		}
	}
 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		dbh = new DB(this);
		pref = getPreferences(Context.MODE_PRIVATE);

		channelList = (ListView)findViewById(R.id.channelList);
		errorText = (TextView)findViewById(R.id.errorText);
		channelList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String cid = ((TextView)view.findViewById(R.id.cid)).getText().toString();
				try {
					Log.d(LOG_TAG, "Video CID=" + cid);
					String url = getVideoUrl(cid);
					Log.d(LOG_TAG, "Video URL=" + url);
					play(url);
				} catch (Exception e){
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	protected void onStart() {
		super.onStart();
		try {
			ensureChannelsUp2Date();
		} catch (Exception e){
			e.printStackTrace();
		}

	    String[] from = new String[]{"name", "cid"};
	    int[] to = new int[] { R.id.name, R.id.cid };
	    SQLiteDatabase db = dbh.getReadableDatabase();
	    Cursor c = db.rawQuery("SELECT rowid _id, * FROM channels ORDER BY name", null);
		SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
	            R.layout.chan_list_item, c, from, to, 0);
		channelList.setAdapter(adapter);
		db.close();
	}
	
	protected void ensureChannelsUp2Date() throws Exception {
		Long lastCheck  = pref.getLong(LAST_CHAN_UPDATE, 0);
		Long now = new Date().getTime();
		if (lastCheck + MIN_CHAN_UPDATE_INTERVAL > now){
			Log.d(LOG_TAG, "No channel update needed, last update " + lastCheck);
			return;
		}
		Log.d(LOG_TAG, "Updating channels, last update " + lastCheck);
		loadChannelList();
		pref.edit().putLong(LAST_CHAN_UPDATE, now).apply();
	}
  
	protected void loadChannelList() throws Exception{
		String url = "http://tv.mits.lv/";
		SQLiteDatabase db = dbh.getWritableDatabase();
		db.delete("channels", null, null);
		ContentValues cv = new ContentValues();
		String html = doDownload(url, "Loading channel list");
		Pattern p = Pattern.compile("<a href=\"\\?q=(.*?)\">(.*?)</a>");
		Matcher m = p.matcher(html);
		for (int i=0; m.find(); i++){
			if (i==0) continue;
			if (i%2 == 0){
			    cv.put("cid", m.group(1));
			    db.insert("channels", null, cv);
			}else{
				cv.put("name", m.group(2));
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	

}

/*
 * TODO:
 * 
 * 1. Favorites
 * 2. Icon
 * 
 * */
