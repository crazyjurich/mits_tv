package com.example.mitstv;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class downloadTask extends AsyncTask<String, Integer, AsyncTaskResult<String>> {
	private static final String TAG = "MitsTV_downloadTask";
	private static DefaultHttpClient hcli;
	private static String LOGIN_URL = "http://tv.mits.lv/index.php?u=test&p=test";
	private ProgressDialog pdia;

	downloadTask(Context c, String message){
		super();
		pdia = new ProgressDialog(c);
		pdia.setMessage(message);
	}

	@Override
	protected void onPreExecute(){ 
		super.onPreExecute();
		if (pdia != null){
			pdia.show();
		}
	}
	
    @Override
    protected void onPostExecute(AsyncTaskResult<String> result) {
		if (pdia != null){
			pdia.dismiss();
		}
    }
    
    protected void doLogin() throws IOException {
    	Log.d(TAG, "doLogin");
    	hcli.execute(new HttpGet(LOGIN_URL));
    }

    protected String doDownload(String url) throws IOException{
    	InputStream is = hcli.execute(new HttpGet(url)).getEntity().getContent();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int length = 0;
		while ((length = is.read(buffer)) != -1) {
			baos.write(buffer, 0, length);
		}
		return baos.toString();
    }

    protected AsyncTaskResult<String> doInBackground(String... urls) {
    	Log.d(TAG, "doInBackground");
		try{
	    	if (hcli == null){
	    		// When app is initialized
				hcli = new DefaultHttpClient();
				doLogin();
	    	}
	    	String res = doDownload(urls[0]);
	    	if (res.contains("password")){
	    		// Logged off, try again
	    		doLogin();
	    		res = doDownload(urls[0]);
	    	}
	    	return new AsyncTaskResult<String>(res);
		} catch(IOException e){
			return new AsyncTaskResult<String>(e);
		}
    }
}
